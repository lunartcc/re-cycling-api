export enum UserTypeEnum {
  USER = 'USER',
  COMPANY = 'COMPANY',
  POINT = 'POINT'
}
