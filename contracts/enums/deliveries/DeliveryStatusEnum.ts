export enum DeliveryStatusEnum {
  ABERTA = 'ABERTA',
  FECHADA = 'FECHADA',
  FINALIZADA = 'FINALIZADA'
}
