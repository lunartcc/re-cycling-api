import Hash from '@ioc:Adonis/Core/Hash'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import AuthValidator from 'App/Validators/auth/AuthValidator'

export default class AuthController {
  async login({ auth, request, response }: HttpContextContract) {
    try {
      const { email, password } = await request.validate(AuthValidator)

      const user = await User.findByOrFail('email', email)

      if (!(await Hash.verify(user.password, password))) {
        return response.badRequest('Invalid credentials')
      }

      const token = await auth.use('api').attempt(email, password, {
        expiresIn: '10years',
      })

      return response.json({ user, token })
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  async logout({ auth, response }: HttpContextContract) {
    try {
      await auth.use('api').revoke()
      return response.json({
        revoked: true,
      })
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }
}
