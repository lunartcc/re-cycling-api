import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import UserPhone from 'App/Models/UserPhone'
import CreateUserPhoneValidator from 'App/Validators/userPhones/CreateUserPhoneValidator'

export default class UserPhonesController {
  public async index({ response }: HttpContextContract) {
    try {
      const phones = await UserPhone.all()

      return response.json(phones)
    } catch (error) {
      console.error(error)
      return response.status(500).json(error)
    }
  }

  public async show({ request, response }: HttpContextContract) {
    const id = request.param('id')
    try {
      const phone = await UserPhone.query().where({ id }).preload('user').firstOrFail()
      return response.json(phone)
    } catch (error) {
      console.error(error)
      return response.status(error.status).json(error)
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const { phone, userId } = await request.validate(CreateUserPhoneValidator)

      const userPhone = await UserPhone.create({ phone, userId })

      return response.status(201).json(await UserPhone.findBy('phone', phone))
    } catch (error) {
      console.error(error)
      return response.status(error.status).json(error)
    }
  }

  public async update({ request, params, response }: HttpContextContract) {
    const { id } = params
    try {
      const { phone, userId } = await request.validate(CreateUserPhoneValidator)

      await UserPhone.query().where({ id }).update({ phone, userId })

      return response.status(204)
    } catch (error) {
      console.error(error)
      return response.status(error.status).json(error)
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    const { id } = params
    try {
      const userPhone = await UserPhone.findOrFail(id)

      await userPhone.delete()
    } catch (error) {
      console.error(error)
      return response.status(error.status).json(error)
    }
  }
}
