import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import User from 'App/Models/User'
import Point from 'App/Models/Point'
import PointPhone from 'App/Models/PointPhone'
import Item from 'App/Models/Item'
import CreatePointValidator from 'App/Validators/points/CreatePointValidator'
import UpdatePointValidator from 'App/Validators/points/UpdatePointValidator'

export default class PointsController {
  public async index({ response }: HttpContextContract) {
    try {
      const points = await Point.query()
        .preload('user')
        .preload('phone')
        .preload('deliveries', (query) => {
          query.preload('items', (queryIem) => {
            queryIem.preload('item')
          })
        })
        .preload('items')

      return response.json(points)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async create({}: HttpContextContract) {}

  public async store({ response, request }: HttpContextContract) {
    try {
      const {
        name,
        email,
        password,
        latitude,
        longitude,
        uf,
        city,
        image,
        phone,
        items,
        openingHours,
        details,
      } = await request.validate(CreatePointValidator)

      if (!image) {
        let user = await User.create({
          email,
          password,
          type: 'POINT',
        })

        user = (await User.findBy('email', email)) as User

        let point = await Point.create({
          userId: user.id,
          name,
          latitude,
          longitude,
          uf,
          city,
          openingHours,
          details,
        })

        point = (await Point.findBy('userId', user.id)) as Point

        await PointPhone.create({
          pointId: point.id,
          phone,
        })

        await point.related('items').attach(items)

        return response
          .status(201)
          .json(
            await Point.query()
              .where({ id: point.id })
              .preload('user')
              .preload('phone')
              .preload('items')
              .first()
          )
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${name.toLowerCase().split(' ')[0]}-${new Date().getMilliseconds()}.${
        image.extname
      }`
      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      let user = await User.create({
        email,
        password,
        type: 'POINT',
      })

      user = (await User.findBy('email', email)) as User

      let point = await Point.create({
        userId: user.id,
        name,
        latitude,
        longitude,
        uf,
        city,
        openingHours,
        details,
        image: `/uploads/${filename}`,
      })

      point = (await Point.findBy('userId', user.id)) as Point

      await PointPhone.create({
        pointId: point.id,
        phone,
      })

      await point.related('items').attach(items)

      return response
        .status(201)
        .json(
          await Point.query()
            .where({ id: point.id })
            .preload('user')
            .preload('phone')
            .preload('items')
            .first()
        )
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async show({ params, response }: HttpContextContract) {
    const { id } = params
    try {
      const point = await Point.query()
        .where({ id })
        .preload('user')
        .preload('phone')
        .preload('deliveries', (query) => {
          query.preload('items', (queryIem) => {
            queryIem.preload('item')
          })
        })
        .preload('items')
        .firstOrFail()

      return response.json(point)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async edit({}: HttpContextContract) {}

  public async update({ request, params, response }: HttpContextContract) {
    const { id } = params
    try {
      const { name, email, latitude, longitude, uf, city, image, openingHours, details } =
        await request.validate(UpdatePointValidator)

      const point = await Point.query().where({ id }).preload('user').firstOrFail()

      if (email) {
        await User.query()
          .where({
            id: point.user!.id,
          })
          .update({ email })
      }

      if (!image) {
        return response.status(204).json(
          await Point.query().where({ id: point.id }).update({
            name,
            latitude,
            longitude,
            uf,
            city,
            openingHours,
            details,
          })
        )
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${
        name?.toLowerCase().split(' ')[0] || point.name.toLowerCase().split(' ')[0]
      }-${new Date().getMilliseconds()}.${image.extname}`
      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      return response.status(204).json(
        await Point.query()
          .where({ id: point.id })
          .update({
            name,
            latitude,
            longitude,
            uf,
            city,
            openingHours,
            details,
            image: `/uploads/${filename}`,
          })
      )
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async destroy({}: HttpContextContract) {}
}
