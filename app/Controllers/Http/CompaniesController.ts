import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import User from 'App/Models/User'
import Company from 'App/Models/Company'
import CreateCompanyValidator from 'App/Validators/companies/CreateCompanyValidator'
import UpdateCompanyValidator from 'App/Validators/companies/UpdateCompanyValidator'

export default class CompaniesController {
  public async index({ request, response }: HttpContextContract) {
    try {
      const companies = await Company.query().preload('user').preload('coupons')

      return response.json(companies)
    } catch (error) {
      console.error(error)
      return response.status(500).json(error)
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const { name, email, password, cnpj, image } = await request.validate(CreateCompanyValidator)

      if (!image) {
        let user = await User.create({
          email,
          password,
          type: 'COMPANY',
        })

        user = (await User.findBy('email', email)) as User

        await Company.create({
          userId: user.id,
          name,
          cnpj,
        })

        return response
          .status(201)
          .json(await Company.query().where({ cnpj }).preload('user').firstOrFail())
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${name.toLowerCase().split(' ')[0]}-${new Date().getMilliseconds()}.${
        image.extname
      }`

      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      let user = await User.create({
        email,
        password,
        type: 'COMPANY',
      })

      user = (await User.findBy('email', email)) as User

      await Company.create({
        userId: user.id,
        name,
        cnpj,
        image: `/uploads/${filename}`,
      })

      return response
        .status(201)
        .json(await Company.query().where({ cnpj }).preload('user').firstOrFail())
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async show({ request, response }: HttpContextContract) {
    const id = request.param('id')
    try {
      const company = await Company.query()
        .where({ id })
        .preload('user')
        .preload('coupons')
        .firstOrFail()
      return response.json(company)
    } catch (error) {
      console.error(error)
      return response.status(error.status).json(error)
    }
  }

  public async update({ request, params, response }: HttpContextContract) {
    const { id } = params
    try {
      const { name, email, cnpj, image } = await request.validate(UpdateCompanyValidator)

      const company = await Company.query().where({ id }).preload('user').firstOrFail()

      if (email) {
        await User.query()
          .where({
            id: company.user!.id,
          })
          .update({ email })
      }

      if (!image) {
        return response.status(204).json(
          await Company.query()
            .where({
              id,
            })
            .update({ name, cnpj })
        )
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${
        name?.toLowerCase().split(' ')[0] || company.name.toLowerCase().split(' ')[0]
      }-${new Date().getMilliseconds()}.${image.extname}`

      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      return response.status(204).json(
        await Company.query()
          .where({
            id,
          })
          .update({ name, cnpj, image: `/uploads/${filename}` })
      )
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async destroy() {}
}
