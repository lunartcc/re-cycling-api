import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import CreateUserValidator from 'App/Validators/users/CreateUserValidator'
import UpdateUserValidator from 'App/Validators/users/UpdateUserValidator'
import UserPhone from 'App/Models/UserPhone'
import User from 'App/Models/User'
import UserInfo from 'App/Models/UserInfo'

export default class UsersController {
  public async index({ response }: HttpContextContract) {
    try {
      const users = await UserInfo.query()
        .preload('user', (query) => {
          query.preload('deliveries').preload('coupons', (queryCoupon) => {
            queryCoupon.preload('company')
          })
        })
        .preload('phone')
      return response.json(users)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async show({ request, response }: HttpContextContract) {
    const id = request.param('id')
    try {
      const user = await UserInfo.query()
        .where({ id })
        .preload('user', (query) => {
          query.preload('deliveries').preload('coupons', (queryCoupon) => {
            queryCoupon.preload('company')
          })
        })
        .preload('phone')

      return response.json(user)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      const { name, email, password, latitude, longitude, uf, city, image, phone } =
        await request.validate(CreateUserValidator)

      if (!image) {
        let user = await User.create({
          email,
          password,
          type: 'USER',
        })

        user = (await User.findBy('email', email)) as User

        let userInfo = await UserInfo.create({
          userId: user.id,
          name,
          latitude,
          longitude,
          uf,
          city,
        })

        userInfo = (await UserInfo.findBy('userId', user.id)) as UserInfo

        await UserPhone.create({
          userInfoId: userInfo.id,
          phone,
        })

        return response
          .status(201)
          .json(
            await UserInfo.query()
              .where({ id: userInfo.id })
              .preload('user')
              .preload('phone')
              .first()
          )
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${name.toLowerCase().split(' ')[0]}-${new Date().getMilliseconds()}.${
        image.extname
      }`
      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      let user = await User.create({
        email,
        password,
        type: 'USER',
      })

      user = (await User.findBy('email', email)) as User

      let userInfo = await UserInfo.create({
        userId: user.id,
        name,
        latitude,
        longitude,
        uf,
        city,
        image: `/uploads/${filename}`,
      })

      userInfo = (await UserInfo.findBy('userId', user.id)) as UserInfo

      await UserPhone.create({
        userInfoId: userInfo.id,
        phone,
      })

      return response
        .status(201)
        .json(
          await UserInfo.query().where({ id: userInfo.id }).preload('user').preload('phone').first()
        )
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async update({ request, params, response }: HttpContextContract) {
    const { id } = params
    try {
      const { name, email, latitude, longitude, uf, city, image } = await request.validate(
        UpdateUserValidator
      )

      const userInfo = await UserInfo.query().where({ id }).preload('user').firstOrFail()

      if (email) {
        await User.query()
          .where({
            id: userInfo.user!.id,
          })
          .update({ email })
      }

      if (!image) {
        return response.status(204).json(
          await UserInfo.query()
            .where({
              id,
            })
            .update({ name, latitude, longitude, uf, city })
        )
      }

      if (!image.isValid) {
        return response.badRequest(image.errors)
      }

      const filename = `${
        name?.toLowerCase().split(' ')[0] || userInfo.name.toLowerCase().split(' ')[0]
      }-${new Date().getMilliseconds()}.${image.extname}`
      await image.move(Application.tmpPath('uploads'), {
        name: filename,
      })

      return response.status(204).json(
        await UserInfo.query()
          .where({
            id,
          })
          .update({ name, latitude, longitude, uf, city, image: `/uploads/${filename}` })
      )
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async destroy() {}
}
