import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Coupon from 'App/Models/Coupon'
import CreateCouponValidator from 'App/Validators/coupons/CreateCouponValidator'
import UpdateCouponValidator from 'App/Validators/coupons/UpdateCouponValidator'
import { DateTime } from 'luxon'

export default class CouponsController {
  public async index({ params, response }: HttpContextContract) {
    const { companyId } = params
    try {
      const coupons = await Coupon.query()
        .where({ companyId })
        .whereNull('deletedAt')
        .preload('user')
      return response.json(coupons)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async store({ request, params, response }: HttpContextContract) {
    const { companyId } = params
    try {
      const { coupons } = await request.validate(CreateCouponValidator)

      await Coupon.createMany(
        coupons.map(({ name, code, value, status, expiresAt }) => ({
          name,
          code,
          companyId,
          value,
          status,
          expiresAt: expiresAt as any,
        }))
      )

      return response.status(201)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async show({ params, response }: HttpContextContract) {
    const { companyId, id } = params
    try {
      const coupon = await Coupon.query()
        .where({ id, companyId })
        .whereNull('deletedAt')
        .preload('user')
        .firstOrFail()
      return response.json(coupon)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async update({ request, params, response }: HttpContextContract) {
    const { id, companyId } = params
    try {
      const { name, code, userId, expiresAt, status, value } = await request.validate(
        UpdateCouponValidator
      )

      await Coupon.query()
        .where({ id, companyId })
        .update({
          name,
          code,
          userId,
          value,
          status,
          expiresAt: expiresAt as any,
        })

      return response.status(204)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    const { id } = params
    try {
      await Coupon.query().where({ id }).update({ deletedAt: new Date() })

      return response.status(200)
    } catch (error) {
      console.error(error)
      return response.status(error.status || 500).json(error)
    }
  }
}
