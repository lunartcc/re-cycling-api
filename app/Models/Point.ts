import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeSave,
  belongsTo,
  BelongsTo,
  column,
  HasMany,
  hasMany,
  HasOne,
  hasOne,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import User from './User'
import Item from './Item'
import PointPhone from './PointPhone'
import Delivery from './Delivery'

export default class Point extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public userId: string

  @belongsTo(() => User, {
    foreignKey: 'userId',
  })
  public user: BelongsTo<typeof User>

  @column()
  public name: string

  @column()
  public image: string

  @column()
  public latitude: string

  @column()
  public longitude: string

  @column()
  public uf: string

  @column()
  public city: string

  @column()
  public openingHours: string

  @column()
  public details: string

  @hasOne(() => PointPhone, {
    foreignKey: 'pointId',
  })
  public phone: HasOne<typeof PointPhone>

  @hasMany(() => Delivery)
  public deliveries: HasMany<typeof Delivery>

  @manyToMany(() => Item, {
    pivotTable: 'point_items',
  })
  public items: ManyToMany<typeof Item>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(point: Point) {
    point.id = uuid()
  }
}
