import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeSave,
  BelongsTo,
  belongsTo,
  column,
  HasMany,
  hasMany,
} from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import User from './User'
import Point from './Point'
import DeliveryItem from './DeliveryItem'

export default class Delivery extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public userId: string

  @belongsTo(() => User, {
    foreignKey: 'userId',
  })
  public user: BelongsTo<typeof User>

  @column()
  public pointId: string

  @belongsTo(() => Point, {
    foreignKey: 'pointId',
  })
  public point: BelongsTo<typeof Point>

  @column()
  public weight: number

  @column()
  public status: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @hasMany(() => DeliveryItem)
  public items: HasMany<typeof DeliveryItem>

  @beforeSave()
  public static generatePrimaryKeyWithUUID(delivery: Delivery) {
    delivery.id = uuid()
  }
}
