import { DateTime } from 'luxon'
import { BaseModel, beforeSave, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import UserInfo from './UserInfo'

export default class UserPhone extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public phone: number

  @column()
  public userInfoId: string

  @belongsTo(() => UserInfo)
  public userInfo: BelongsTo<typeof UserInfo>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(userPhone: UserPhone) {
    userPhone.id = uuid()
  }
}
