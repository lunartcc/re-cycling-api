import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeSave,
  BelongsTo,
  belongsTo,
  column,
  hasOne,
  HasOne,
} from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import User from './User'
import UserPhone from './UserPhone'

export default class UserInfo extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public userId: string

  @belongsTo(() => User, {
    foreignKey: 'userId',
    localKey: 'id',
  })
  public user: BelongsTo<typeof User>

  @column()
  public name: string

  @column()
  public image: string

  @column()
  public latitude: string

  @column()
  public longitude: string

  @column()
  public uf: string

  @column()
  public city: string

  @hasOne(() => UserPhone)
  public phone: HasOne<typeof UserPhone>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(userInfo: UserInfo) {
    userInfo.id = uuid()
  }
}
