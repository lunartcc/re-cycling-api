import { DateTime } from 'luxon'
import { BaseModel, beforeSave, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import Delivery from './Delivery'
import Item from './Item'

export default class DeliveryItem extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public deliveryId: string

  @belongsTo(() => Delivery, {
    foreignKey: 'deliveryId',
  })
  public delivery: BelongsTo<typeof Delivery>

  @column()
  public itemId: string

  @belongsTo(() => Item, {
    foreignKey: 'itemId',
  })
  public item: BelongsTo<typeof Item>

  @column()
  public weight: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(deliveryItem: DeliveryItem) {
    deliveryItem.id = uuid()
  }
}
