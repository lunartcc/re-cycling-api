import { DateTime } from 'luxon'
import { BaseModel, beforeSave, column, manyToMany, ManyToMany } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import Point from './Point'

export default class Item extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public name: string

  @column()
  public image: string

  @manyToMany(() => Point, {
    pivotTable: 'point_items',
  })
  public points: ManyToMany<typeof Point>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(item: Item) {
    item.id = uuid()
  }
}
