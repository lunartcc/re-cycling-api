import { DateTime } from 'luxon'
import { BaseModel, beforeSave, belongsTo, BelongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import Point from './Point'

export default class PointPhone extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public phone: number

  @column()
  public pointId: string

  @belongsTo(() => Point, {
    foreignKey: 'pointId',
  })
  public point: BelongsTo<typeof Point>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(pointPhone: PointPhone) {
    pointPhone.id = uuid()
  }
}
