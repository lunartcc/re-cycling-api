import { DateTime } from 'luxon'
import { BaseModel, beforeSave, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import Company from './Company'
import User from './User'

export default class Coupon extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public companyId: string

  @belongsTo(() => Company, {
    foreignKey: 'companyId',
  })
  public company: BelongsTo<typeof Company>

  @column()
  public userId: string

  @belongsTo(() => User, {
    foreignKey: 'userId',
  })
  public user: BelongsTo<typeof User>

  @column()
  public name: string

  @column()
  public code: string

  @column()
  public value: string

  @column()
  public status: boolean

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @column.dateTime()
  public expiresAt: DateTime

  @beforeSave()
  public static generatePrimaryKeyWithUUID(coupon: Coupon) {
    coupon.id = uuid()
  }
}
