import { DateTime } from 'luxon'
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasOne,
  HasOne,
  hasMany,
  HasMany,
} from '@ioc:Adonis/Lucid/Orm'
import { v4 as uuid } from 'uuid'
import UserInfo from './UserInfo'
import Company from './Company'
import Point from './Point'
import Coupon from './Coupon'
import Delivery from './Delivery'

export default class users extends BaseModel {
  @column({ isPrimary: true })
  public id: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public emailVerified: boolean

  @column({ serializeAs: null })
  public email_verified_token?: string

  @column()
  public preRegister: boolean

  @column()
  public type: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @column.dateTime()
  public deletedAt: DateTime

  @hasOne(() => UserInfo, {
    foreignKey: 'userId',
  })
  public userInfo: HasOne<typeof UserInfo>

  @hasOne(() => Company, {
    foreignKey: 'userId',
  })
  public company: HasOne<typeof Company>

  @hasOne(() => Point, {
    foreignKey: 'userId',
  })
  public point: HasOne<typeof Point>

  @hasMany(() => Coupon, {
    foreignKey: 'userId',
  })
  public coupons: HasMany<typeof Coupon>

  @hasMany(() => Delivery, {
    foreignKey: 'userId',
  })
  public deliveries: HasMany<typeof Delivery>

  @beforeSave()
  public static async hashPassword(user: users) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @beforeSave()
  public static generatePrimaryKeyWithUUID(user: users) {
    user.id = uuid()
  }
}
