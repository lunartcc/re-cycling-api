import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import UserInfoFactory from 'Database/factories/UserInfoFactory'


export default class UserInfoSeeder extends BaseSeeder {
  public async run () {
    try {
      await UserInfoFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
