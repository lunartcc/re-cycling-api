import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import UserPhoneFactory from 'Database/factories/UserPhoneFactory'

export default class UserPhoneSeeder extends BaseSeeder {
  public async run () {
    try {
      await UserPhoneFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
