import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Item from 'App/Models/Item'

export default class ItemSeeder extends BaseSeeder {
  public async run () {
    try {
      const items = [
        {
          name: 'Vidro',
          image: '/vidro.png'
        },
        {
          name: 'Orgânico',
          image: '/organico.png'
        },
        {
          name: 'Papelão',
          image: '/papelao.png'
        },
        {
          name: 'Plástico',
          image: '/plastico.png'
        }
      ]

      await Item.createMany(items)
    } catch (error) {
      console.error(error)
    }
  }
}
