import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import CouponFactory from 'Database/factories/CouponFactory'

export default class CouponSeeder extends BaseSeeder {
  public async run() {
    try {
      await CouponFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
