import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import User from 'App/Models/User'
import { UserTypeEnum } from 'Contracts/enums/users/UserTypeEnum'

export default class UserSeeder extends BaseSeeder {
  public async run() {
    await User.create({
      email: 'admin@admin.com',
      password: '123456',
      type: UserTypeEnum.USER,
    })
  }
}
