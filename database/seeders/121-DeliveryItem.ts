import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeliveryItemFactory from 'Database/factories/DeliveryItemFactory'

export default class DeliveryItemSeeder extends BaseSeeder {
  public async run () {
    try {
      await DeliveryItemFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
