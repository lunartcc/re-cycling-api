import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Item from 'App/Models/Item'
import Point from 'App/Models/Point'

export default class PointItemSeeder extends BaseSeeder {
  public async run () {
    try {
      const point = await Point.first()
      const item = await Item.first()

      await point!.related('items').attach([item!.id])
    } catch (error) {
      console.error(error)
    }
  }
}
