import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import PointFactory from 'Database/factories/PointFactory'

export default class PointSeeder extends BaseSeeder {
  public async run () {
    try {
      await PointFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
