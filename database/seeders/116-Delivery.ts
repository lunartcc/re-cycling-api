import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import DeliveryFactory from 'Database/factories/DeliveryFactory'

export default class PointSeeder extends BaseSeeder {
  public async run () {
    try {
      await DeliveryFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
