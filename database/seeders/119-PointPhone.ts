import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import PointPhoneFactory from 'Database/factories/PointPhoneFactory'

export default class PointPhoneSeeder extends BaseSeeder {
  public async run () {
    try {
      await PointPhoneFactory.createMany(5)
    } catch (error) {
      console.error(error)
    }
  }
}
