import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class DeliveryItems extends BaseSchema {
  protected tableName = 'delivery_items'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('delivery_id').references('id').inTable('deliveries').onDelete('CASCADE')
      table.uuid('item_id').references('id').inTable('items').onDelete('CASCADE')
      table.decimal('weight').nullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true }).defaultTo(this.now())
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
