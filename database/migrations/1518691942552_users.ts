import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import { UserTypeEnum } from 'Contracts/enums/users/UserTypeEnum'

export default class UsersSchema extends BaseSchema {
  protected tableName = 'users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.string('email', 255).unique().notNullable()
      table.string('password', 180).notNullable()
      table.string('remember_me_token').nullable()
      table.boolean('email_verified').notNullable().defaultTo(0)
      table.string('email_verified_token').nullable()
      table.boolean('pre_register')
      table.enum('type', Object.values(UserTypeEnum))

      /**
       * Uses timestampz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
      table.timestamp('deleted_at').nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
