import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Coupons extends BaseSchema {
  protected tableName = 'coupons'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('company_id').references('id').inTable('companies').onDelete('CASCADE')
      table.uuid('user_id').references('id').inTable('users').nullable()
      table.string('name').notNullable()
      table.string('code').unique().notNullable()
      table.enum('value', [1, 2, 3, 4, 5])
      table.boolean('status').defaultTo(true)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
      table.timestamp('deleted_at').nullable()
      table.timestamp('expires_at').nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
