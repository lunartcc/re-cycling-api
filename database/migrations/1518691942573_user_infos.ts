import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserInfos extends BaseSchema {
  protected tableName = 'user_infos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users').onDelete('CASCADE')
      table.string('name').notNullable()
      table.string('image').nullable()
      table.string('latitude').notNullable()
      table.string('longitude').notNullable()
      table.string('uf', 2).notNullable()
      table.string('city').notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
      table.timestamp('deleted_at').nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
