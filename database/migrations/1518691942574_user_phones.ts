import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UserPhones extends BaseSchema {
  protected tableName = 'user_phones'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('user_info_id').references('id').inTable('user_infos').onDelete('CASCADE')
      table.bigInteger('phone').unique().notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
      table.timestamp('deleted_at').nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
