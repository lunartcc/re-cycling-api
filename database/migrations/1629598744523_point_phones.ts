import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PointPhones extends BaseSchema {
  protected tableName = 'point_phones'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('point_id').references('id').inTable('points').onDelete('CASCADE')
      table.bigInteger('phone').unique().notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps(true, true)
      table.timestamp('deleted_at').nullable()
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
