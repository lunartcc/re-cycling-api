import BaseSchema from '@ioc:Adonis/Lucid/Schema'
import { DeliveryStatusEnum } from 'Contracts/enums/deliveries/DeliveryStatusEnum'

export default class Deliveries extends BaseSchema {
  protected tableName = 'deliveries'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary()
      table.uuid('user_id').references('id').inTable('users').onDelete('CASCADE')
      table.uuid('point_id').references('id').inTable('points').onDelete('CASCADE')
      table.decimal('weight').nullable()
      table.enum('status', Object.values(DeliveryStatusEnum))
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps(true, true)
       table.timestamp('deleted_at').nullable()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
