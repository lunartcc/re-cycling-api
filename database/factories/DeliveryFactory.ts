import Factory from '@ioc:Adonis/Lucid/Factory'
import Delivery from 'App/Models/Delivery'
import Point from 'App/Models/Point'
import User from 'App/Models/User'
import { DeliveryStatusEnum } from 'Contracts/enums/deliveries/DeliveryStatusEnum'
import _ from 'lodash'

const DeliveryFactory = Factory.define(Delivery, async ({ faker }) => {
  const { id: userId } = _.sample(await User.all())!
  const { id: pointId } = _.sample(await Point.all())!

  const deliveryStatusEnum = Object.values(DeliveryStatusEnum)

  return {
    userId,
    pointId,
    weight: faker.datatype.number(100),
    status: _.sample(deliveryStatusEnum),
  }
}).build()

export default DeliveryFactory
