import Factory from '@ioc:Adonis/Lucid/Factory'
import _ from 'lodash'
import UserPhone from 'App/Models/UserPhone'
import UserInfo from 'App/Models/UserInfo'

const UserPhoneFactory = Factory.define(UserPhone, async ({ faker }) => {
  const { id: userInfoId } = _.sample(await UserInfo.all())!

  return {
    userInfoId,
    phone: faker.datatype.number(),
  }
}).build()

export default UserPhoneFactory
