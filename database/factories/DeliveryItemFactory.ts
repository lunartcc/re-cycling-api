import Factory from '@ioc:Adonis/Lucid/Factory'
import DeliveryItem from 'App/Models/DeliveryItem'
import Delivery from 'App/Models/Delivery'
import Item from 'App/Models/Item'
import _ from 'lodash'

const DeliveryFactory = Factory.define(DeliveryItem, async ({ faker }) => {
  const { id: deliveryId } = _.sample(await Delivery.all())!
  const { id: itemId } = _.sample(await Item.all())!

  return {
    deliveryId,
    itemId,
    weight: faker.datatype.number(100),
  }
}).build()

export default DeliveryFactory
