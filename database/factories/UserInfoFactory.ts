import Factory from '@ioc:Adonis/Lucid/Factory'
import UserInfo from 'App/Models/UserInfo'
import User from 'App/Models/User'
import _ from 'lodash'

const UserInfoFactory = Factory.define(UserInfo, async ({ faker }) => {
  const { id: userId } = _.sample(await User.all())!

  return {
    userId,
    name: faker.name.findName(),
    image: faker.image.avatar(),
    uf: faker.address.stateAbbr(),
    city: faker.address.cityName(),
    latitude: faker.address.latitude(),
    longitude: faker.address.longitude(),
  }
}).build()

export default UserInfoFactory
