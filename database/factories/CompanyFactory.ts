import Factory from '@ioc:Adonis/Lucid/Factory'
import Company from 'App/Models/Company'
import User from 'App/Models/User'
import _ from 'lodash'

const CompanyFactory = Factory.define(Company, async ({ faker }) => {
  const { id: userId } = _.sample(await User.all())!

  return {
    userId,
    name: faker.company.companyName(),
    image: faker.image.business(),
    cnpj: faker.datatype.number(14),
  }
}).build()

export default CompanyFactory
