import Factory from '@ioc:Adonis/Lucid/Factory'
import Point from 'App/Models/Point'
import User from 'App/Models/User'
import _ from 'lodash'

const PointFactory = Factory.define(Point, async ({ faker }) => {
  const { id: userId } = _.sample(await User.all())!

  return {
    userId,
    name: faker.company.companyName(),
    image: faker.image.business(),
    uf: faker.address.stateAbbr(),
    city: faker.address.cityName(),
    latitude: faker.address.latitude(),
    longitude: faker.address.longitude(),
    openingHours: 'Segunda à sábado das 12:00 até 18:00',
    details: faker.lorem.paragraph(),
  }
}).build()

export default PointFactory
