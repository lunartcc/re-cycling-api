import Factory from '@ioc:Adonis/Lucid/Factory'
import _ from 'lodash'
import PointPhone from 'App/Models/PointPhone'
import Point from 'App/Models/Point'

const PointPhoneFactory = Factory.define(PointPhone, async ({ faker }) => {
  const { id: pointId } = _.sample(await Point.all())!

  return {
    pointId,
    phone: faker.datatype.number(),
  }
}).build()

export default PointPhoneFactory
