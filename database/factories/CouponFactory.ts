import Factory from '@ioc:Adonis/Lucid/Factory'
import Coupon from 'App/Models/Coupon'
import _ from 'lodash'
import Company from 'App/Models/Company'

const CouponFactory = Factory.define(Coupon, async ({ faker }) => {
  const { id: companyId } = _.sample(await Company.all())!
  const couponValueEnum = ['1', '2', '3', '4', '5']

  return {
    companyId,
    name: faker.name.findName(),
    code: faker.datatype.hexaDecimal(),
    value: _.sample(couponValueEnum),
    status: faker.datatype.boolean(),
  }
}).build()

export default CouponFactory
