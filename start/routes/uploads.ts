import Route from '@ioc:Adonis/Core/Route'

Route.get('uploads/:filename', 'UploadsController.show')
