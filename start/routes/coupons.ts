import Route from '@ioc:Adonis/Core/Route'

Route.resource('companies/:companyId/coupons', 'CouponsController').except(['create', 'edit'])
