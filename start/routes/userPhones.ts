import Route from '@ioc:Adonis/Core/Route'

Route.resource('phones', 'UserPhonesController').except(['create', 'edit'])
