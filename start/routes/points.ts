import Route from '@ioc:Adonis/Core/Route'

Route.resource('points', 'PointsController').except(['create', 'edit'])
