import Route from '@ioc:Adonis/Core/Route'

Route.resource('items', 'ItemsController').except(['create', 'edit'])
